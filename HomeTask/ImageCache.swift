//
//  ImageCache.swift
//  HomeTask
//
//  Created by TestUser on 11/05/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    static let sharedImageCache = ImageCache()
    var imageArray = [NSUUID:UIImage?]()
 
    func getImage(imageID: NSUUID, completion: @escaping(UIImage?)->Void){
        let isAlreadyLoaded = imageArray.keys.contains(imageID)
        if (isAlreadyLoaded) {
            completion(self.imageArray[imageID]!)
        } else {
            DownloadManager.sharedDownloadManager.downloadNextImage(imageID: imageID, completion: {(image : UIImage?) in
                self.saveImage(image: image, imageID: imageID)
                completion(image)
            })
        }
    }
    
    func stopDownloading(imageID: NSUUID) {
        DownloadManager.sharedDownloadManager.stopDownloading(imageID: imageID)
    }
    
    func saveImage(image: UIImage?, imageID: NSUUID) {
        self.imageArray[imageID] = image
    }
    
    func clearCache() {
        //todo: clear local storage
        DownloadManager.sharedDownloadManager.cancellAll()
        for imageID in imageArray.keys{
            DownloadManager.sharedDownloadManager.deleteImageFromDisk(imageID: imageID)
        }
        imageArray.removeAll()
    }
    
}
