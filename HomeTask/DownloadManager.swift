//
//  DownloadManager.swift
//  test2
//
//  Created by TestUser on 23/04/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//
import Foundation
import UIKit

class DownloadManager{
    
    static let sharedDownloadManager = DownloadManager()
    
    var downloadQueue: OperationQueue
    
    //init a queue
    init() {
        self.downloadQueue = {
            let queue = OperationQueue()
            queue.name = "Download queue"
            queue.maxConcurrentOperationCount = 1
            return queue
        }()
    }
    
    func cancellAll() {
        downloadQueue.cancelAllOperations()
    }
    
    func stopDownloading(imageID: NSUUID) {
        for operatin in self.downloadQueue.operations {
            if (operatin as! Downloader).imageID == imageID {
                operatin.cancel()
            }
        }
    }
    
    //download an image
    func downloadNextImage(imageID: NSUUID, completion: @escaping (UIImage?) -> Void){
        if (isLoadedFromDisk(imageID: imageID, completion: completion)) {
            return
        }
        self.downloadWithQueue(imageID: imageID, completion: completion)
    }
    
    func downloadWithQueue(imageID: NSUUID, completion: @escaping (UIImage?) -> Void) {
        let downloader = Downloader(imageID: imageID)
        downloader.completionBlock = {
            if !downloader.isCancelled {
                self.saveOnDisk(imageID: imageID, image: downloader.image)
                completion(downloader.image)
            }
        }
        downloadQueue.addOperation(downloader)
    }
    
    func isLoadedFromDisk(imageID: NSUUID, completion: @escaping (UIImage?) -> Void) -> Bool {
        let (imageFound,image) = imageFoundOnDisk(imageID: imageID)
        if (imageFound) {
            completion(image)
            return true
        }
        return false
    }
    
    func getURL(imageID: NSUUID) -> URL {
        let docDir = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let imageURL = docDir.appendingPathComponent(String(describing: imageID))
        return imageURL
    }
    
    func saveOnDisk(imageID: NSUUID, image: UIImage?) {
        if image == nil {
            return
        }
        let imageURL = getURL(imageID: imageID)
        let imageData = UIImagePNGRepresentation(image!)!
        do{
            try imageData.write(to: imageURL)
        }
        catch{
            print(error.localizedDescription)
        }

    }
    
    func imageFoundOnDisk(imageID: NSUUID) -> (Bool, UIImage?) {
        let imageURL = getURL(imageID: imageID)
        let newImage: UIImage? = UIImage(contentsOfFile: imageURL.path)
        return ((newImage != nil),newImage)
    }
    
    func deleteImageFromDisk(imageID: NSUUID) {
        let fileManager = FileManager.default
        let imageURL = getURL(imageID: imageID)
        do {
            try fileManager.removeItem(at: imageURL)
        }
        catch{
            print(error.localizedDescription)
        }
    }
}

