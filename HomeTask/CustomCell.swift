//
//  CustomCell.swift
//  HomeTask
//
//  Created by TestUser on 15/05/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell{
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellView: UIImageView!
    
    var imageData: ImageData?{
        didSet{
            self.cellLabel.text = imageData?.getText()
            ImageCache.sharedImageCache.getImage(imageID: (imageData?.imageID)!){
                image in self.updateCellImage(imageID: (self.imageData?.imageID)!, image: image)
            }    
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellLabel.numberOfLines = 0
        self.cellLabel.lineBreakMode = .byWordWrapping
    }
    
    func prepareFor(_ imageData: ImageData) {
        self.imageData = imageData
    }
    
    func updateCellImage(imageID: NSUUID, image: UIImage?) {
        if self.imageData?.imageID == imageID {
            DispatchQueue.main.async{
                self.cellView.image = image
            }
        }
    }
    
    override func prepareForReuse(){
        super.prepareForReuse()
        clearCell()
        //pause downloading
        ImageCache.sharedImageCache.stopDownloading(imageID: (self.imageData?.imageID)!)
    }
    
    func clearCell() {
        self.cellView.image = nil
        self.cellLabel.text = nil
    }
}
