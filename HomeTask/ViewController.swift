//
//  ViewController.swift
//  test2
//
//  Created by TestUser on 14/04/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var userDefaults = UserDefaults.standard
    var model: [ImageData] = []{
        didSet{
            saveModelOnDisk()
        }        
    }
    let cellIdentifier = "customCell"
    let modelIdentifier = "model"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadModel()
    }
    
    func loadModel() {
        let decoded:Data?  = userDefaults.object(forKey: modelIdentifier) as! Data?
        if decoded != nil {
            model = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [ImageData]
        }
    }
    
    //click on the "+" button
    @IBAction func addItem(_ sender: UIButton) {
        self.model.append(ImageData())
        addElementOnTable()
    }
    
    func saveModelOnDisk() {
        let storageData: Data = NSKeyedArchiver.archivedData(withRootObject: model)
        userDefaults.set(storageData, forKey: modelIdentifier)
        userDefaults.synchronize()
    }
    
    //add a new picture on the TableView
    func addElementOnTable() {
        tableView.beginUpdates()
        let indexPath = IndexPath(row: self.model.count-1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }
    
    //click on the Clear button
    @IBAction func deleteAllItems(_ sender: UIButton) {
        model.removeAll()
        ImageCache.sharedImageCache.clearCache()
        self.tableView.reloadData()
    }
    
    //number of raws equals number of pictures
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CustomCell
        cell.prepareFor(model[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imageData = self.model[indexPath.row]
        imageData.rename()
        self.model[indexPath.row] = imageData
        //self.saveModelOnDisk()
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

