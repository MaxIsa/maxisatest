//
//  NameGenerator.swift
//  test2
//
//  Created by TestUser on 18/04/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//

import Foundation

enum Names: UInt32{
    case Agrauding
    case Ekoocwood
    case Gukrose
    case Igaaginia
    case Israk
    case Odermont
    case Qaumery
    case Qupholk
    case Shona
    case Ylalo
    
    static func getEnum(stringValue: String) -> Names {
        switch stringValue {
        case "Agrauding":
            return self.Agrauding
        case "Ekoocwood":
            return self.Ekoocwood
        case "Gukrose":
            return self.Gukrose
        case "Igaaginia":
            return self.Igaaginia
        case "Israk":
            return self.Israk
        case "Odermont":
            return self.Odermont
        case "Qaumery":
            return self.Qaumery
        case "Qupholk":
            return self.Qupholk
        case "Shona":
            return self.Shona
        case "Ylalo":
            return self.Ylalo
        default:
            return self.Agrauding
        }    
    }
    
    private static let _count: Names.RawValue = {
        // find the maximum enum value
        var maxValue: UInt32 = 0
        while let _ = Names(rawValue: maxValue) {
            maxValue += 1
        }
        return maxValue
    }()
    
    //return a new random name
    static func getRandomName() -> Names {
        let rand = arc4random_uniform(_count)
        return Names(rawValue: rand)!
    }
}
