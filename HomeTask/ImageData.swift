//
//  ImageData.swift
//  HomeTask
//
//  Created by TestUser on 23/05/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//

import Foundation

class ImageData: NSObject, NSCoding{
    var name:Names
    var date:Date
    var imageID:NSUUID
    
    override init() {
        self.name = Names.getRandomName()
        self.date = Date()
        self.imageID = NSUUID()
    }
    
    init(imageID: NSUUID, name: Names, date: Date) {
        self.imageID = imageID
        self.name = name
        self.date = date
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        let imageID = aDecoder.decodeObject(forKey: "imageID") as! NSUUID
        let stringName = aDecoder.decodeObject(forKey: "name") as! String
        let name = Names.getEnum(stringValue: stringName)
        let date = aDecoder.decodeObject(forKey: "date") as! Date
        self.init(imageID: imageID, name: name, date: date)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.imageID, forKey: "imageID")
        aCoder.encode(String(describing: self.name), forKey: "name")
        aCoder.encode(self.date, forKey: "date")
    }
    
     func rename() {
        self.name = Names.getRandomName()
    }
    
    func getText() -> String {
        return String(describing: name) + "\n" + getDateAsString()
    }
    
    func getDateAsString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        return dateFormatter.string(from: date)
    }
}
