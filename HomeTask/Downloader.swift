//
//  Downloader.swift
//  test2
//
//  Created by TestUser on 18/04/2017.
//  Copyright © 2017 TestUser. All rights reserved.
//

import Foundation
import UIKit

class Downloader: Operation  {
    var image:UIImage?
    var imageID:NSUUID
    static let url = URL(string: "https://lorempixel.com/1400/1400/nature/")!
    
    init(imageID: NSUUID) {
        image = nil
        self.imageID = imageID
    }
    
    override func main(){
        var imageData: NSData?
        if self.isCancelled {
            return
        }
        imageData = NSData(contentsOf: Downloader.url)
        if imageData != nil && !self.isCancelled {
            self.image = UIImage(data:imageData! as Data)
         }
    }
    
    
    
}
